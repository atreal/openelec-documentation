.. _manuel_utilisateur:

#######################
Manuel de l'utilisateur
#######################

.. toctree::
    :numbered:

    preambule/index.rst
    utilisateur_saisies/index.rst
    consultation/index.rst
    editions/index.rst
    traitements/index.rst
    module_multi/index.rst
    administration_parametrage/index.rst
