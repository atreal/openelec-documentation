.. _procuration_saisie:

###########
Procuration
###########

Préambule
=========

Cet écran est accessible via le menu (:menuselection:`Saisie Électeur --> Procuration`). Tous les détails sur les procurations sont disponibles dans le chapitre :ref:`module_procuration`.


Saisie des informations
=======================

.. figure:: a_saisie_procuration.png

    Formulaire d'ajout d'une procuration

Le formulaire permet de saisir les informations suivantes sur la procuration :

* **Identification**

    * Statut : *statut de la demande de procuration (A transmettre ou refusée)*
    * Motif de refus : *Motif de refus uniquement accessible pour les demandes refusés*

* **Électeurs (mandant et mandataire)**

    * Mandant/Mandataire hors commune : *Case à cocher si le mandant/mandataire n'est pas rattaché à la commune pour pouvoir le chercher sur le REU via son INE*
    * Mandant : *demandeur de la procuration*
    * Mandataire : *personne désignée pour voter à la place du mandant*

* **Validité de la procuration**

    * Du / Au : *les dates de validité de la procuration au format JJ/MM/AAAA (par exemple : pour une procuration valide uniquement le 26/05/2019, il faut saisir du 26/05/2019 au 26/05/2019)*

* **Établissement de la procuration**

    * Nom et prénom de l'autorité d'établissement : *le nom et le prenom de l'autorité qui a dressé l'acte de procuration*
    * Autorité d'établissement  : *la qualité de l'autorité qui a dressé l'acte de procuration (par exemple : Gendarmerie)*
    * Lieu d'établissement (En France) : *code insee et nom de la commune où a été établie la procuration*
    * Date d'établissement : *date au format JJ/MM/AAAA d'établissement de la procuration*

* **Notes**

    * Notes : *Informations complémentaires concernant la demande de procuration*


Validation des informations
===========================

Une fois toutes les informations saisies, vous pouvez valider le
formulaire pour enregistrer la procuration.

Le refus d'une procuration entraine l'affichage sur le tableau d'un lien vers
l'édition d'un courrier de refus adressé

Message de Vérification : Un mandant ne donne qu’une procuration pour une
même période. Un mandataire a au plus 2 mandats pour une même période dont
au moins un hors France. Un électeur en centre de vote ne peut pas donner
procuration. Dans ces cas la procuration peut tout de même être enregistrée si
elle est refusée. Des outils permettent de vérifier les procurations et de les
éditer. Ils se trouvent dans l’écran du menu « Traitement Procurations &
Mentions ».
