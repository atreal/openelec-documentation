###################
Module "Préfecture"
###################

Depuis la mise en place du Répertoire Électoral Unique au 1er janvier 2019, le module "Préfecture" n'existe plus dans openElec. Ce module permettait principalement le transfert des données au portail e.LISTELEC DEMATERIALISATION DES LISTES ELECTORALES https://elistelec.interieur.gouv.fr/. Les préfectures ont un accès direct aux données des communes dans le Répertoire Électorale Unique.

