.. _traitements:

###########
Traitements
###########

.. figure:: a_menu-rubrik-traitement.png

   Menu - Rubrique 'Traitement'

.. toctree::

   module_commission.rst
   module_reu.rst
   module_election.rst
   module_procuration.rst
   module_traitement_j-5.rst
   module_traitement_fin_d_annee.rst
   module_cartes_en_retour.rst
   module_jury_d_assises.rst
   module_refonte.rst
   module_archivage.rst
   module_redecoupage.rst

   module_prefecture.rst
