#######################
Module "Jury d'Assises"
#######################

Le module "Jury d'Assises" est accessible via le menu (:menuselection:`Traitement --> Jury d'Assises`).

.. image:: a_module_jury_d_assises_menu.png

.. contents::


Préambule
---------

Les jurés d'assises peuvent être gérés par le logiciel pour faire le tirage
aléatoire et sortir les étiquettes ou le listing. Vous pouvez aussi modifier
manuellement les informations de juré d'assises d'un électeur directement
depuis sa fiche électeur via le menu (:menuselection:`Consultation --> Liste des électeurs`).

.. figure:: a_module_jury_d_assises.png

    Ecran du module : Jury d'Assises

Paramétrer le nombre de jurés
-----------------------------

Pour cela, vous devez tout d'abord saisir le nombre de juré voulu pour chaque
canton. Par la suite ce nombre ne vous sera plus demandé.

Pour inscrire ce nombre de juré :

- cliquez sur l'onglet "Paramétrage",
- cliquez sur sur la flèche pour accéder à la fiche du nombre de juré par canton,
- cliquez sur sur l'action modifier pour accéder au formulaire de modification,
- insérez le nombre de juré choisi,
- cliquez sur le bouton "Modifier" pour enregistrer les changements.

.. figure:: a_module_jury_d_assises_parametrage_collectivite.png

    Rubrique Parametrage : Collectivite

Effectuer un tirage au sort
---------------------------

Pour tirer au sort le jury rendez-vous dans la rubrique " Traitement
/ Jury d'Assises " . Cliquez ensuite sur le bouton " Tirage aléatoire ".

Attention, être tiré au sort par la mairie ne signifie pas forcément
être membre du " jury d’assise " car une nouvelle
sélection a lieu au niveau de la préfecture :

* Contrôle de la profession.

* Lien de parenté avec l’accusé.

* Refus de la part de l’électeur.

Confirmer un membre du jury
---------------------------

Pour définir un électeur tiré au sort comme un membre effectif du jury,
rendez-vous sur le tableau des électeurs tirés au sort puis éditez sa fiche en cliquant sur le bouton
modifier (représenté par une icone de crayon).

.. note:: Il est possible d'éditer ces information depuis la fiche électeur
          via le lien "Informations pour le jury d'assises" du menu contextuel.

Enfin remplissez la section " Juré d'assises ". Le champ " Date préfecture "
correspond à la date à laquelle la préfecture a donné son accord pour la
sélection de cet électeur.

.. figure:: a_module_jury_d_assises_jure_effectif.png

    Rubrique Saisie : Carte en retour / Jure

Module "Suppléant Jury D'Assises"
---------------------------------

Pour activer ce module, il faut ajouter, depuis le menu (:menuselection:`Administration & Paramétrage --> Général --> paramètre `), un paramètre :ref:`**option_module_jures_suppleants**<administration_parametrage>` et lui donner la valeur true.

Une fois le module actif, les Jurés Suppléants pourront également être géré depuis le menu
(:menuselection:`Consultation --> Liste des électeurs`).


Le paramétrage se fait depuis l'onglet "Paramétrage".
Le nombre de suppléants qui seront tirés au sort, pour chaque canton, est déterminé via le champs *nombre de suppléants*.
Pour le remplir, il faut, pour chaque canton :

- cliquez sur l'onglet "Paramétrage",
- cliquez sur sur la flèche pour accéder à la fiche du nombre de juré par canton,
- cliquez sur sur l'action modifier pour accéder au formulaire de modification,
- insérez le nombre de juré choisi,
- insérez le nombre de juré suppléant choisi,
- cliquez sur le bouton "Modifier" pour enregistrer les changements.

.. figure:: a_module_suppleant_jury_d_assises_parametrage_collectivite.png

Le tirage au sort est fait depuis l'onglet "Traitement / Jury d'Assises".
Il est possible de tirer au sort les jurés et les suppléant en cliquant sur le bouton "Tirage aléatoire" de la rubrique correspondante.

.. note:: Le tirage au sort des titulaires remet à 0 la liste des suppléants. Les suppléants doivent donc toujours être tirés en premier et les suppléants en second.

.. figure:: a_module_suppleant_jury_d_assises_tirage_sort.png

Une fois les suppléants tirés au sort, la confirmation des suppléants en tant que membre actif se fait depuis l'onglet "Liste Préparatoire courante".
Depuis ce menu, la colonne *jury* permet d'identifier s'il s'agit d'un titulaire ou d'un suppléant.
L'édition des informations des suppléants se fait de la même manière que celle des titulaires.

.. figure:: a_module_jury_d_assises_suppleant_jure_effectif.png
