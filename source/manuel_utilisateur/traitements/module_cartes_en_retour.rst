#######################
Module Cartes en Retour
#######################

Le module Cartes en Retour est accessible via le menu
(:menuselection:`Traitement --> Cartes en Retour`).

.. image:: a_module_cartes_en_retour_menu.png

.. contents::

Les cartes en retour peuvent être gérées par le logiciel pour répertorier
la liste des cartes d'électeurs retournées par la poste. Vous pouvez aussi
modifier manuellement les informations de cartes en retour d'un électeur
directement depuis sa fiche d'électeur en cliquant sur "Enregistrer une
carte en retour pour cet électeur".

Saisie par Lots
###############

Depuis cet onglet il est possible soit d'inscrire le numéro
d'électeur, soit de scanner le code inscrit sur la carte, si vous avez
une douchette.
Il est également possible de choisir la date de saisie de la carte en retour.
Si une date est saisie elle sera visible sur la fiche de l'électeur concerné.

.. figure:: a_module_cartes_en_retour.png

    Ecran du module : Cartes en Retour

Liste des électeurs
###################

Cet onglet permet de voir la liste des électeurs ayant une carte en retour.

Éditions
########

Cet onglet vous permet d'accéder aux différentes éditions du module carte en retour.

Édition par bureau de vote
--------------------------

Registre de toutes les cartes en retour, dont les électeurs sont rattachés à la liste et au bureau de vote sélectionnés, triés par ordre alphabétique de nom d'électeur.


Épuration
#########

Cet onglet permet de réaliser l'épuration des cartes en retour.
L'épuration des cartes en retour se fait en fonction de leur date de saisie. Le traitement se fait sur toutes les listes.

* Si aucune date n'est sélectionnée, toutes les cartes en retour seront épurées.
* Si la date de début et la date de fin sont remplies, les cartes en retour dont la date de saisie est comprise dans cet intervalle seront supprimées.
* Si uniquement la date de début est sélectionnée, toutes les cartes en retour dont la date de saisie est postérieure à cette date seront supprimées.
* Si uniquement une date de fin est remplie, toutes les cartes en retour n'ayant pas de date de saisie ou ayant une date de saisie antérieure seront supprimées.
 